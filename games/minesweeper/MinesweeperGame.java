package com.javarush.games.minesweeper;

import com.javarush.engine.cell.Color;
import com.javarush.engine.cell.Game;

import java.util.ArrayList;
import java.util.List;


public class MinesweeperGame extends Game {
    private static final int SIDE = 9;
    private GameObject[][] gameField = new GameObject[SIDE][SIDE];
    private int countMinesOnField;
    private static final String MINE = "\uD83D\uDCA3";
    private static final String FLAG = "\uD83D\uDEA9";
    private int countFlags;
    private boolean isGameStopped;
    private int countClosedTiles = SIDE * SIDE;
    private int score;

    @Override
    public void initialize() {
        setScreenSize(SIDE, SIDE);
        createGame();

    }

    private void createGame()
    {
        for (int y = 0; y < SIDE; y++)
        {
            for (int x = 0; x < SIDE; x++)
            {
                boolean isMine = getRandomNumber(10) < 1;
                if (isMine)
                {
                    countMinesOnField++;
                }
                gameField[y][x] = new GameObject(x, y, isMine);
                setCellColor(x, y, Color.ORANGE);
                setCellValue(x, y, "");
            }
        }
        countMineNeighbors();
        countFlags = countMinesOnField;
    }

    private List<GameObject> getNeighbors(GameObject gameObject)
    {
        List<GameObject> result = new ArrayList<>();
        for (int y = gameObject.y - 1; y <= gameObject.y + 1; y++) { //берем "верхнего соседа" и идем до "нижнего"
            for (int x = gameObject.x - 1; x <= gameObject.x + 1; x++) { //берем "левого соседа" и идем до "правого"
                if (y < 0 || y >= SIDE)
                {
                    continue;
                }
                if (x < 0 || x >= SIDE)
                {
                    continue;
                }
                if (gameField[y][x] == gameObject)
                {
                    continue;
                }
                result.add(gameField[y][x]);
            }
        }
        return result;
    }


    private void countMineNeighbors()
    {
        int countMineNeighbors = 0;
        for (int y = 0;y <SIDE;y++)
        {
            for (int x = 0; x<SIDE;x++)
            {
                if (gameField[y][x].isMine == false)
                {
                    List<GameObject>  gameObjectList = getNeighbors(gameField[y][x]);
                    for (GameObject gameObject: gameObjectList)
                    {
                        if (gameObject.isMine)
                        {
                            gameField[y][x].countMineNeighbors++;
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onMouseLeftClick(int x, int y)
    {
        if (isGameStopped == true)
        {
            restart();
        }
        else
        {
            openTile(x, y);
        }
    }


    private void openTile(int x, int y)
    {
        if (!gameField[y][x].isOpen && !gameField[y][x].isFlag && !isGameStopped)
        {
            gameField[y][x].isOpen = true;
            countClosedTiles--;

            if (!gameField[y][x].isMine)
            {
                score += 5;
                setScore(score);
            }

            if (countMinesOnField == countClosedTiles && !gameField[y][x].isMine)
            {
                win();
            }

            if (gameField[y][x].isMine)
            {
                setCellValueEx(x, y, Color.RED, MINE);
                gameOver();
            }
            else
            {
                if (gameField[y][x].countMineNeighbors != 0)
                {
                    setCellNumber(x, y, gameField[y][x].countMineNeighbors);
                    setCellColor(x, y, Color.GREENYELLOW);
                }
                else
                {
                    setCellValue(x, y, "");
                    setCellColor(x, y, Color.TEAL);
                    List<GameObject> list = getNeighbors(gameField[y][x]);
                    for (GameObject each : list)
                    {
                        if (!each.isOpen)
                        {
                            openTile(each.x, each.y);
                        }
                    }
                }
            }
        }
    }


    @Override
    public void onMouseRightClick(int x, int y)
    {
        markTile(x, y);
    }


    private void markTile(int x, int y)
    {
        if (!gameField[y][x].isOpen && !isGameStopped)
        {
            if (gameField[y][x].isFlag)
            {
                gameField[y][x].isFlag = false;
                countFlags++;
                setCellColor(x, y, Color.ORANGE);
                setCellValue(x, y, "");
            }
            else if (countFlags != 0)
            {
                gameField[y][x].isFlag = true;
                countFlags--;
                setCellColor(x, y, Color.FORESTGREEN);
                setCellValue(x, y, FLAG);
            }
        }
    }


    private void gameOver()
    {
        isGameStopped = true;
        showMessageDialog(Color.BLACK, "GAME OVER!", Color.BLUEVIOLET, 40);
    }


    private void win()
    {
        isGameStopped = true;
        showMessageDialog(Color.BLACK, "Ты красава!", Color.GREEN, 50);
    }


    private void restart()
    {
        isGameStopped = false;
        countClosedTiles = SIDE * SIDE;
        score = 0;
        setScore(score);
        countMinesOnField = 0;
        createGame();
    }
}